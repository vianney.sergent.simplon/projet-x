import React from 'react';
import ReactDOM from 'react-dom';
import Table from 'react-bootstrap/Table';

function Userlist(props) {
    let users = props.users;
   
    return (
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>email</th>
                </tr>
            </thead>

            {users.map((user, index) => {

                return<tbody key={index}>
                    <tr>
                        <td>{user.id}</td>
                        <td>{user.email}</td>
                    </tr>
                </tbody>

            })}
        </Table>
    )
}

window.render = function (props) {
    ReactDOM.render(<Userlist users={props} />, document.getElementById('root'));
}

