Brief projet "Le monde d'après" 
===============================

Suite à la crise du coronavirus, nous souhaitons vous proposer le développement d'une plateforme permettant aux citoyens du monde entier d'imaginer le monde d'après. Notre système est à bout de souffle, l'hyper optimisation de celui-ci le rend fragile, quelles sont les modifications à apporter pour le rendre plus résilient ?

L'objectif principal est de proposer une plateforme collaborative offrant aux internautes un moyen d'imaginer, de débattre, de fédérer,... pour la construction d'un nouveau modèle de société. Une idée simple peut amener de grands changements, devenez acteurs de votre futur !

Quelques idées en vrac pour alimenter votre réflexion :

* une plateforme **open-source** permettant le vote en ligne de lois proposées par les citoyens
* une plateforme permettant la création d'une constitution collaborative
* une plateforme du type "les décodeurs citoyens"
* une plateforme permettant de recenser un ensemble de produits et leur équivalent francais
* une plateforme permettant d'identifier les entreprises locales par secteur d'activité

## Objectifs

* Prise en main plus approfondie de Symfony
* Identifier problématique(s) et proposer une solution 
* Imaginer et concevoir une application
* Travailler en groupe de façon efficace
* Gestion de projet (tâches, planning,..)

## Compétences visées

* Compétence 1, maquettage : niveau transposer
* Compétence 2, web statique : niveau transposer
* Compétence 5, créer une base de donnée : niveau transposer
* Compétence 6, développer les composants d'accès aux données : niveau adapter/transposer
* Compétence 7, développer la partie back-end d'une application : niveau transposer

## Modalités pédagogiques

Ce projet se déroulera sur 4 semaines (18 jours) en 2 sprints, le travail sera réalisé par groupe de 3 personnes.

## Etapes

1. Définir un projet
2. Réalisation de wireframes
3. Définir les différentes tâches dans un **Trello**
4. Traiter les tâches définies

## Tâches

### Sprint 1 : jusqu'au 29 mai
Vous l'aurez compris le sujet étant totalement libre, vous serez en charge de définir les tâches de votre projet. Néanmoins quelques fonctionnalités devront **obligatoirement être implémentées** :

* Upload de fichiers
* Envoie de mails,... (sms ?)
* Application React alimentée par du JSON

> Attention, l'objectif de ce projet n'est pas de faire du javascript mais simplement de réaliser le lien entre une application React et Symfony. Il ne faudra pas passer trop de temps sur l'application React, pour se faire nous vous proposons simplement l'implémentation d'[autocomplete de material-ui](https://material-ui.com/components/autocomplete/)

### Sprint 2 : vous sera fourni le 01/06/20
