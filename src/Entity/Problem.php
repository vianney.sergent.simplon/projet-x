<?php

namespace App\Entity;

use App\Repository\ProblemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProblemRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"title"},
 *     errorPath="title",
 *     message="problem.unique"
 * )

 */
class Problem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="problem.null")
     * @Assert\Regex(
     *     pattern="/[\{\}\;]/",
     *     match=false,
     *     message="problem.regex"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="problem.null")
     * @Assert\Length(
     *      min = 15,
     *      max = 255,
     *      minMessage = "problem.limit.min",
     *      maxMessage = "problem.limit.max",
     *      allowEmptyString = false)
     * 
     * @Assert\Regex(
     *     pattern="/[\{\}\;]/",
     *     match=false,
     *     message="problem.regex"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startVote;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endVote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *     maxSize = "600k",
     *     mimeTypesMessage = "problem.file"
     * )
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="problems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Tag::class, inversedBy="problem", cascade={"persist", "remove"})
     * @Assert\NotNull
     */
    private $tag;

    /**
     * @ORM\OneToMany(targetEntity=Solution::class, mappedBy="problem")
     * @ORM\OrderBy({"id"="ASC"})
     */
    private $solutions;

    public function __construct()
    {
        $this->solutions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getStartVote(): ?\DateTimeInterface
    {
        return $this->startVote;
    }

    public function setStartVote(?\DateTimeInterface $startVote): self
    {
        $this->startVote = $startVote;

        return $this;
    }

    public function getEndVote(): ?\DateTimeInterface
    {
        return $this->endVote;
    }

    public function setEndVote(?\DateTimeInterface $endVote): self
    {
        $this->endVote = $endVote;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTag(): ?Tag
    {
        return $this->tag;
    }

    public function setTag(?Tag $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return Collection|Solution[]
     */
    public function getSolutions(): Collection
    {
        return $this->solutions;
    }

    public function addSolution(Solution $solution): self
    {
        if (!$this->solutions->contains($solution)) {
            $this->solutions[] = $solution;
            $solution->setProblem($this);
        }

        return $this;
    }

    public function removeSolution(Solution $solution): self
    {
        if ($this->solutions->contains($solution)) {
            $this->solutions->removeElement($solution);
            // set the owning side to null (unless already changed)
            if ($solution->getProblem() === $this) {
                $solution->setProblem(null);
            }
        }

        return $this;
    }

    public function __toString() {
        return $this->getTitle();
    }

        /**
        * @ORM\PrePersist
        */
        public function setCreatedAtValue()
        {
            $this->createDate = new \DateTime();
            $this->state = "start";
        }
}
