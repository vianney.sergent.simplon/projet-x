<?php

namespace App\Email;

use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class EmailSend
{


  public function sendEmail(MailerInterface $mailer)
  {
    $email = (new Email())
      ->from('sarah.ghanem.simplon@gmail.com')
      ->to('sarah.ghanem.simplon@gmail.com')
      //->cc('exemple@mail.com') 
      //->bcc('exemple@mail.com')
      //->replyTo('test42@gmail.com')
      ->priority(Email::PRIORITY_HIGH)
      ->subject('I love Me')
      // If you want use text mail only
      ->text('Lorem ipsum...')
      // Raw html
      ->html('<h1>Lorem ipsum</h1> <p>...</p>');
      $mailer->send($email);
  }
}
