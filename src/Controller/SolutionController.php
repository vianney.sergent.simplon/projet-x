<?php

namespace App\Controller;

use App\Entity\Problem;
use App\Entity\Solution;
use App\Form\SolutionType;
use App\Repository\VoteRepository;
use App\Repository\SolutionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/solution")
 */
class SolutionController extends AbstractController
{   

    public function __construct(Security $security)
{
    
    $this->security = $security;
}

    /**
     * @Route("/{id}/new", name="solution_new", methods={"GET","POST"})
     */
    public function newSolution(Request $request, Problem $problem): Response
    {
        $solution = new Solution();  
        $form = $this->createForm(SolutionType::class, $solution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->security->getUser();
            $solution->setUser($user);
            $solution->setProblem($problem);
            $entityManager->persist($solution);
            $entityManager->flush();

            return $this->redirectToRoute('problem_index');
        }

        return $this->render('solution/new.html.twig', [
            'solution' => $solution,
            'form' => $form->createView(),
            'problem'=> $problem,
        ]);
    }

     /**
     * @Route("/{id}/show", name="solution_show", methods={"GET"})
     */
    public function show( VoteRepository $voteRepo, int $id, SolutionRepository $solutionRepo): Response
    {
        $user = $this->security->getUser();
        $solution = $solutionRepo->findOneBy(['id' => $id]);
        $vote = $voteRepo->findOneBy(['user'=>$user, 'solution'=>$solution]);
        $voteFor = count($voteRepo->findBy(['solution'=>$solution,'resulteVote'=>true]));
        $voteAgainst = count($voteRepo->findBy(['solution'=>$solution,'resulteVote'=>false]));
        return $this->render('solution/show.html.twig', [
            'solution'=>$solution,
            'vote'=>$vote,
            'voteFor'=>$voteFor,
            'voteAgainst'=>$voteAgainst,
        ]);
    }
}
