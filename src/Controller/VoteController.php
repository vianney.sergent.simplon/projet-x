<?php

namespace App\Controller;

use App\Entity\Vote;
use App\Repository\VoteRepository;
use App\Repository\SolutionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/{id}/vote")
 */
class VoteController extends AbstractController
{

public function __construct(Security $security)
{
    
    $this->security = $security;
}

    /**
     * @Route("/for", name="vote_for", methods={"GET"})
     */
    public function voteFor(VoteRepository $voteRepo, int $id, SolutionRepository $solutionRepo): Response
    {
        $user = $this->security->getUser();
        $solution = $solutionRepo->findOneBy(['id' => $id]);
        $vote = $voteRepo->findOneBy(['user'=>$user, 'solution'=>$solution]);
        if($vote){
            return $this->redirectToRoute('home');
        }
        if($user){
            $entityManager = $this->getDoctrine()->getManager();
            $vote = new Vote();
            $vote->setResulteVote(true);
            $vote->setCreateDate( new \DateTime());
            $vote->setUser($user);
            $vote->setSolution($solution);
            $entityManager->persist($vote);
            $entityManager->flush();
        }

        return $this->redirectToRoute('problem_index');
    }

     /**
     * @Route("/against", name="vote_against", methods={"GET"})
     */
    public function voteAgainst(VoteRepository $voteRepo, int $id, SolutionRepository $solutionRepo): Response
    {
        $user = $this->security->getUser();
        $solution = $solutionRepo->findOneBy(['id' => $id]);
        $vote = $voteRepo->findOneBy(['user'=>$user, 'solution'=>$solution]);
        if($vote){
            return $this->redirectToRoute('home');
        }
        if($user){
            $entityManager = $this->getDoctrine()->getManager();
            $vote = new Vote();
            $vote->setResulteVote(false);
            $vote->setCreateDate( new \DateTime());
            $vote->setUser($user);
            $vote->setSolution($solution);
            $entityManager->persist($vote);
            $entityManager->flush();
        }

        return $this->redirectToRoute('problem_index');
    }
    
}