<?php

namespace App\Form;

use App\Entity\Problem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ProblemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
              'translation_domain' => 'problem_create',
              'label' => 'problem_create.title',
            ])
            ->add('description',  
              TextType::class, [
              'translation_domain' => 'problem_create',
              'label' => 'problem_create.description',
            ])
            ->add('image', FileType::class, [
              'translation_domain' => 'problem_create',
              'label' => 'problem_create.piece',
            ])
            ->add('tag')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Problem::class,
        ]);
    }
}
