<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $pswEncoder) {

      $this->pswEncoder = $pswEncoder; 

    }
    public function load(ObjectManager $manager)
    {
          foreach ($this->getUserData() as [$fName, $lName, $email, $password, $roles]) {
    
          $user = new User(); 
          $user->setfirstName($fName);
          $user->setlastName($lName);
          $user->setEmail($email); 
          $user->setPassword($this->pswEncoder->encodePassword($user, $password));
          $user->setRoles($roles);
          $manager->persist($user);
      }
      

    
          $tag = new Tag(); 
          $tag->setName("finances"); 

          $tag1 = new Tag(); 
          $tag1->setName("retraite"); 

          $tag2 = new Tag(); 
          $tag2->setName("social"); 

          $tag3 = new Tag(); 
          $tag3->setName("santé"); 

          $tag4 = new Tag(); 
          $tag4->setName("économie"); 

          $manager->persist($tag);
          $manager->persist($tag1);
          $manager->persist($tag2);
          $manager->persist($tag3);
          $manager->persist($tag4);
      
      $manager->flush();
    }


    private function getUserData(): array

    {
      return 
      [
          ['Ian','Chatenet','Ian@chatenet.com', 'ouais', ['ROLE_ADMIN']],
          ['Brayan','Zbra','Brayan@brayan.com', 'brayan123', ['ROLE_USER']],
          ['Test','mail','test@mail.lol', 'banana', ['ROLE_ADMIN']]
      ];
    }



}
