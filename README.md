###INDIGNEZ VOUS !

"*Suite à la crise du coronavirus, nous souhaitons vous proposer le développement d'une plateforme permettant aux citoyens du monde entier d'imaginer le monde d'après. Notre système est à bout de souffle, l'hyper optimisation de celui-ci le rend fragile, quelles sont les modifications à apporter pour le rendre plus résilient ?
L'objectif principal est de proposer une plateforme collaborative offrant aux internautes un moyen d'imaginer, de débattre, de fédérer,... pour la construction d'un nouveau modèle de société. Une idée simple peut amener de grands changements, devenez acteurs de votre futur !*"

##ENVIRONNEMENT

**PHP >= 7.4.0**

**Node.js >= 12.16.1**

**Composer**

**Yarn**

##CONFIG DATABASE

Faites une copie du fichier **env-exemple** et nommez la **.env**.

Configurez la connexion à votre base de données dans le fichier **.env**.

    DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
    
##INSTALLATION

    composer install
    yarn install or yarn

##CREATE DATABASE

Faites les commandes suivante :

    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    php bin/console doctrine:fixtures:load
#START PROJECT

    yarn encore dev 
    symfony server:start or symfony serve


    